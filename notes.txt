What is a Data Model?

	A data model describes how data is organized and grouped in a database.

	By creating data models, we can anticipate which data will be stored and managed in our database by our chosen DBMS in accordance to the application to be developed.

Data Modelling
	
	Database should have a purpose and its organization must be related to the kind of application we are building.


When we are creating an application, one of the first steps in its creating is determining the Data models:

Scenario:

	A Course Booking system application wherein a user can book into a course


Type: Course Booking System
Description: A course booking system application where a user can book into a course.

Features:
	
	- User Registration
	- User Login/Authentication
		- Authenticated Users:
			- View Course
			- User Enrollment
			- Cancel Enrollment
			- Update Details (with Admin User Verification)
			- Remove Details (with Admin User Verification)

		- Admin User
			- Add Course
			- View/Read Courses
			- Update Courses
			- Archive/De-Activate Course
			- Re-Activate Course

		- All Users (guests,authenticated,admin)
			- View Active Courses
	
Data Model
	
	User Document, Course Document, Transaction/Enrollment Document

	{
		username: "TJ",
		password: "1244"
	},
	{
		email: "romenick@gmail.com",
		password: "1235"
	}

	-Data Models are blueprints for our documents in our database.
	-Data Models are created so that we can follow and structure our data consistently.

	User Data Model

		User {

			id - unique identifier for the document,
			userName,
			firstName,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin

		}

		Course {

			id - Unique Identifier for the Document,
			description,
			name,
			units,
			slots,
			schedule,
			price,
			isActive,
			instructor
		}

		Enrollment {

			id - Unique Identifier for the Document,
			userId - the unique identifier for the enrolled user,
			userName - Optional,
			courseId - The unique identifer for the course,
			courseName- Optional,
			dateEnrolled,
			modeOfPayment,
			status

		}

Model Relationships

	To be able to properly orgainze an application database we should be able to identify the relationships between our models. So that we can establish a relationship between our documents.

	One to One - This relationship means that a model is exclusively related to only one model.

	Referencing: When a document is referencing another document:

	Employee:
	{
		"id": "2023Dev",
		"firstName": "Jack",
		"lastName": "Sullivan",
		"email": "jsDev2023@gmail.com"
	}

	Credentials:
	{
		"id": "creds_01",
		"employee_id": "2023Dev",
		"role": "developer",
		"team": "tech"
	}

	In MongoDB, one to one relationship can be expressed in another way instead of referencing:

		Embedding - embed is to put another document in a document.
			Subdocument - is a document embedded/inside another document.

	Employee 
		{
			"id": "2023Dev",
			"firstName": "Jack",
			"lastName": "Sullivan",
			"email": "jsDev2023@gmail.com",
			"credentials": {

				"id": "creds_01",
				"role": "developer",
				"team": "tech"

			}
		}


	One to Many

		One model is related to multiple other models.
		However, the other models are only related to one.

		Person - Many Email Address

		Email Address - One Person

		Blog Post -> comments

			A blog post can have many comments but each comment should only refer to that single blog post

		Blog: {

			"id": "blog1-23",
			"title": "This is an Awesome Blog!",
			"content": "This is an awesome blog that I created!",
			"createdOn": "11/1/2023",
			"author": "BlogWriter1"
		}

		Comments:

			{
				"id": "blogcomment1",
				"comment": "Awesome Blog!",
				"author": "blogGuy1",
				"blog_id": "blog1-23"
			},
			{
				"id": "blogcomment2",
				"comment": "Meh. Not Awesome at all.",
				"author": "notHater22",
				"blog_id": "blog1-23"
			}

		In MongoDB, one to many relationship can also be expressed in another way:

			Subdocument Array - an array of subdocuments per single parent document

		Blog: {

			"id": "blog1-23",
			"title": "This is an Awesome Blog!",
			"content": "This is an awesome blog that I created!",
			"createdOn": "11/1/2023",
			"author": "BlogWriter1",
			"comments": [

				{
					"id": "blogcomment1",
					"comment": "Awesome Blog!",
					"author": "blogGuy1"
				},
				{
					"id": "blogcomment2",
					"comment": "Meh. Not Awesome at all.",
					"author": "notHater22"
				}

			]

		}

	Many to Many

		Multiple documents are related to multiple documents

		users - courses

		courses - users

		When a many to many relationship is create, for models to relate to each other, an associative entity is created.

		Associative Entity is a model that relates models in the many to many relationship

		user - enrollment - course

		So that a user can relate to a course and so that we can track the enrollment of a user to a course, we have to create the details of the enrollment.

		User {

			"id": "student1",
			"firstName": "Ryan",
			"lastName": "Yumul",
			"email": "ryan.yumul@gmail.com",
			"password": "ryan1234",
			"mobileNumber": "09266772111",
			"isAdmin": false
		}

		Course {
			"id": "course1",
			"name": "Python-Django",
			"description": "Learn the famous Python framework, Django",
			"price": 25000,
			"isActive" true,
		}

		Enrollment {

			"id": "enrollment1"
			"userId": "student1",
			"courseId": "course1",
			"courseName": "Python-Django",
			"dateEnrolled": "11/1/2023",

		}

		In MongoDB, many to many relationship can also be expressed in another way:

			Two-Way Embedding - In two-way embedding, the associative entity is created and embedded in both models/documents

		User {

			"id": "student1",
			"firstName": "Ryan",
			"lastName": "Yumul",
			"email": "ryan.yumul@gmail.com",
			"password": "ryan1234",
			"mobileNumber": "09266772111",
			"isAdmin": false,
			"enrollments": [

				{
					"id": "enrollment1",
					"courseId": "course1",
					"courseName": "Python-Django",
					"dateEnrolled": "11/1/2023",
				}

			]
		}

		Course {

			"id": "course1",
			"name": "Python-Django",
			"description": "Learn the famous Python framework, Django",
			"price": 25000,
			"isActive" true,
			"enrollees": [
					
					{
						"id": "enrollee1"
						"userId": "student1",
						"dateEnrolled": "11/1/2023",
					}

			]
		}


Mini-Activty:
	
	Create a sample document for the following Data Model:

	Product {
		"id": "product1",
		"name": "PS5 - Elden Ring",
		"description": "Fromsoftware's latest soulsborne game.",
		"price": 2950,
		"isActive": true
	}

	User {
		"id": "James",
		"email": "james12@gmail.com",
		"password": "12345semaj",
		"mobileNum": "09223654568",
		"isAdmin": false
	}

	//Create a sample product Document
	//Create a sample User Document